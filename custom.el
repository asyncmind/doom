(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-safe-themes
   '("4aa639a41f19674bfa0f29d0389daca16e4962f9fcb8b8fd089038583ff34085" "e1ef2d5b8091f4953fe17b4ca3dd143d476c106e221d92ded38614266cea3c8b" "01cf34eca93938925143f402c2e6141f03abb341f27d1c2dba3d50af9357ce70" "5036346b7b232c57f76e8fb72a9c0558174f87760113546d3a9838130f1cdb74" "fe00bb593cb7b8c015bb2eafac5bfc82a9b63223fbc2c66eddc75c77ead7c7c1" "cf3d5d77679f7daed6a2c863e4f2e30427d5e375b254252127be9359957502ec" "a3b6a3708c6692674196266aad1cb19188a6da7b4f961e1369a68f06577afa16" "4bca89c1004e24981c840d3a32755bf859a6910c65b829d9441814000cf6c3d0" "c4bdbbd52c8e07112d1bfd00fee22bf0f25e727e95623ecb20c4fa098b74c1bd" "990e24b406787568c592db2b853aa65ecc2dcd08146c0d22293259d400174e37" "5d09b4ad5649fea40249dd937eaaa8f8a229db1cec9a1a0ef0de3ccf63523014" "76bfa9318742342233d8b0b42e824130b3a50dcc732866ff8e47366aed69de11" default))
 '(fci-rule-color "#767676")
 '(helm-buffer-max-length 50)
 '(helm-rg-ripgrep-executable "/usr/sbin/rg")
 '(jdee-db-active-breakpoint-face-colors (cons "#0F1019" "#D85F00"))
 '(jdee-db-requested-breakpoint-face-colors (cons "#0F1019" "#79D836"))
 '(jdee-db-spec-breakpoint-face-colors (cons "#0F1019" "#767676"))
 '(objed-cursor-color "#D83441")
 '(package-selected-packages '(helm-projectile projectile rg helm-rg helm helm-themes))
 '(pdf-view-midnight-colors (cons "#CEDBE5" "#0D0E16"))
 '(rustic-ansi-faces
   ["#0D0E16" "#D83441" "#79D836" "#D8B941" "#3679D8" "#8041D8" "#36D8BD" "#CEDBE5"])
 '(vc-annotate-background "#0D0E16")
 '(vc-annotate-color-map
   (list
    (cons 20 "#79D836")
    (cons 40 "#98cd39")
    (cons 60 "#b8c33d")
    (cons 80 "#D8B941")
    (cons 100 "#d89b2b")
    (cons 120 "#d87d15")
    (cons 140 "#D85F00")
    (cons 160 "#ba5548")
    (cons 180 "#9d4b90")
    (cons 200 "#8041D8")
    (cons 220 "#9d3ca5")
    (cons 240 "#ba3873")
    (cons 260 "#D83441")
    (cons 280 "#bf444e")
    (cons 300 "#a7555b")
    (cons 320 "#8e6568")
    (cons 340 "#767676")
    (cons 360 "#767676")))
 '(vc-annotate-very-old-color nil))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )

(set-face-inverse-video-p 'vertical-border nil)
(set-face-background 'vertical-border (face-background 'default))
;; Set symbol for the border
;; http://stackoverflow.com/questions/18210631/how-to-change-the-character-composing-the-emacs-vertical-border
(set-display-table-slot standard-display-table
                        'vertical-border
                                                (make-glyph-code ?│))
(when (fboundp 'windmove-default-keybindings)
  (windmove-default-keybindings 'control))

(setq
 helm-buffer-max-length 30
 helm-full-frame nil
 helm-adaptive-mode t
 helm-ff-lynx-style-map t
 helm-always-two-windows t
 helm-split-window-in-side-p nil
 )
(setq
   helm-split-window-in-side-p           nil ; open helm buffer inside current window, not occupy whole other window
   helm-move-to-line-cycle-in-source     nil ; move to end or beginning of source when reaching top or bottom of source.
   helm-ff-search-library-in-sexp        t ; search for library in `require' and `declare-function' sexp.
   helm-scroll-amount                    8 ; scroll 8 lines other window using M-<next>/M-<prior>
   helm-ff-file-name-history-use-recentf t
   helm-autoresize-mode t
   helm-M-x-always-save-history t
   helm-always-two-windows t
   helm-ff-lynx-style-map t ;Note that if you define this variable with ‘setq’ your change will  have no effect, use customize insteadi
   helm-boring-buffer-regexp-list
   (quote
    ("\\` " "\\*helm" "\\*helm-mode" "\\*Echo Area" "\\*Minibuf" "\\*vc-"
     "\\*Complet" "\\*magit" "\\*cscope" "\\*epc"))
   helm-boring-file-regexp-list
   (quote
    ("\\.git$" "\\.hg$" "\\.svn$" "\\.CVS$" "\\._darcs$" "\\.la$" "\\.o$" "~$" "\\.pyc$"))
   helm-buffers-fuzzy-matching t
   helm-M-x-fuzzy-match t
   helm-recentf-fuzzy-match t
   helm-mode-fuzzy-match t
   helm-buffers-fuzzy-matching t
   helm-imenu-fuzzy-match t
   helm-locate-fuzzy-match nil
   helm-semantic-fuzzy-match t
   helm-c-ack-version 2
   helm-ff-auto-update-initial-value nil
   helm-ff-file-name-history-use-recentf t
   helm-ff-ido-style-backspace t
   helm-ff-skip-boring-files t
   helm-ff-smart-completion t
   helm-ff-transformer-show-only-basename nil
   helm-findutils-skip-boring-files t
   helm-mode-handle-completion-in-region t
   helm-mode-reverse-history nil
   helm-quick-update t
   helm-reuse-last-window-split-state nil
   helm-match-plugin-mode t
   helm-adaptive-mode t
   helm-full-frame nil
   projectile-completion-system 'helm
   helm-buffer-max-length 30
   helm-ag-use-grep-ignore-list t
   helm-ag-use-agignore t
   helm-truncate-lines t

   helm-mini-default-sources
   (quote
    (
     helm-source-projectile-buffers-list
     helm-source-buffers-list
     helm-source-recentf
     helm-source-buffer-not-found
     helm-source-locate
     ))
   helm-for-files-preferred-list
   (quote
    (
     ;;helm-source-projectile-recentf-list
     helm-source-recentf
     helm-source-projectile-files-list
     ;;helm-source-files-in-current-dir
     ;;helm-source-file-cache
     ;;helm-source-bookmarks
     helm-source-locate
     )))
